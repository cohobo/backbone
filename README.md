## Provided services

The main goal of this configuration is to allow to connect many development
environments and allow the to work simultaneously on different local domains.

Apart from this some useful containers are added.
- [Mailhog](https://github.com/mailhog/MailHog) container allowing you to test outgoing emails
- RabbitMq
- [Imgproxy](https://github.com/imgproxy/imgproxy) to provide on the fly image conversion

## Installation

### STEP 1
Checkout the repository

```git clone git@gitlab.com:cohobo/backbone.git```

### STEP 2

Create external docker network

```docker network create cohobo_dev```

### STEP 3

Create a local domains for Traefik - add to your /etc/hosts line:

```127.0.0.1 traefik.cc home.traefik.cc mailhog.cc```

### STEP 4
Create self-signed ssl cert. 

```openssl req -x509 -nodes -days 10000 -newkey rsa:2048 -keyout ./certs/cert.key -out ./certs/cert.crt```

### STEP 5

Run Traefik. From this point container should run automatically after each reboot.

```docker-compose up -d```

If you installed everything correctly you should be able ta access:

[https://traefik.cc](https://traefik.cc)

### STEP 6 (optional)

If your apps are using ImgProxy for an on the fly image conversion you need to configure
nginx caching reverse proxy server.

Check also my [PHP library for generating ImgProxy Urls ](https://gitlab.com/cohobo/imgproxyphp).

Two example configurations are provided - one with short time caching 
and one with caching disabled. For development the second one seems a good choice.

Simply copy one of provided configurations:

```cp nginx/nginx_example_with_caching.conf nginx/nginx.conf ```

or 

```cp nginx/nginx_example_without_caching.conf nginx/nginx.conf```

Alternatively create your own

That's all folks - you are ready to go ;-)
